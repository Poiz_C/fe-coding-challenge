import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';
import {ItunesMusicInterface} from '../interfaces/itune-music.interface';

@Injectable({
  providedIn: 'root'
})
export class ValuesCentralService {
  searchResultsSignal: Subject<ItunesMusicInterface[]>;
  abortOperationsSignal: Subject<boolean>;
  updateFieldValueSignal: Subject<string>;
  refocusSearchFieldSignal: Subject<null>;
  searchButtonTriggeredStream: Subject<Event>;

  constructor() {
    this.searchResultsSignal = new Subject<ItunesMusicInterface[]>();
    this.abortOperationsSignal = new Subject<boolean>();
    this.updateFieldValueSignal = new Subject<string>();
    this.refocusSearchFieldSignal = new Subject<null>();
    this.searchButtonTriggeredStream = new Subject<Event>();
  }

  broadcastUpdateFieldValueSignal(fieldValue: string): void {
    this.updateFieldValueSignal.next(fieldValue);
  }

  broadcastAbortOperationsSignal(status: boolean): void {
    this.abortOperationsSignal.next(status);
  }

  updateSearchButtonTriggeredStream(event: Event): void {
    this.searchButtonTriggeredStream.next(event);
  }

  broadcastSearchResults(finalResult: ItunesMusicInterface[]) {
    this.searchResultsSignal.next(finalResult);
  }

  broadcastRefocusSearchFieldSignal(): void {
    this.refocusSearchFieldSignal.next(null);
  }
}
