import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import {map, Observable, of} from 'rxjs';
import {catchError, shareReplay} from 'rxjs/operators';
import { ErrorManager } from './error.manager.service';
import {HttpErrorHandlerType} from '../interfaces/generic.types';
import {ItunesMusicInterface, ItunesPayloadInterface} from "../interfaces/itune-music.interface";
import {ValuesCentralService} from "./values-central.service";


@Injectable({
  providedIn: 'root'
})
export class SearchBrokerService {
  tunesEndpoint = 'https://itunes.apple.com/search?term=radiohead';
  private manageError: HttpErrorHandlerType;

  constructor( private http: HttpClient, errorManager: ErrorManager, private vcService: ValuesCentralService) {
    this.manageError = errorManager.createManageError('SearchBrokerService');
  }

  /**
   * GIVEN A KEYWORD REPRESENTING AN ITEM'S FULL OR PARTIAL NAME,
   * FETCHES A COLLECTION OF ITEMS [SONGS] FROM THE SPECIFIED ENDPOINT... [ `this.tunesEndpoint` ]
   */
  fetchSuggestionsByKeyWord(keyword: string): Observable<ItunesMusicInterface[]> {
    keyword = keyword.trim();
    // BAIL EARLY IF WE DON'T HAVE A KEYWORD TO AVOID FETCHING ENTIRE PAYLOAD.
    // HOWEVER, THIS METHOD SHOULD ONLY BE CALLED WITH AN EXPLICIT KEYWORD PARAMETER.
    if (!keyword) {
      return of([]);
    }
    // URL-ENCODE SEARCH-TERM AND ADD IT TO THE HTTP PARAMS AS OPTIONS.
    keyword = keyword.replace(/([\[\].\-+{}()?\/:])/gim, '\\$1');
    const options = keyword ?
      { params: (new HttpParams()).set('term', keyword) } : {};
    return this.http.get<ItunesPayloadInterface>(this.tunesEndpoint, options)
      .pipe(
        shareReplay(),
        map( payload => {
          // LIMIT SELECTION TO ALBUMS ONLY (COLLECTION NAME) - JUST FOR FUN...
          const songsOnly =  payload.results.filter( tune => !!tune.collectionName);
          const uniqueSongsObject: {[key: string]: ItunesMusicInterface} = {};

          // REMOVE SONGS WITH DUPLICATE TRACK-NAMES
          songsOnly.forEach( song => {
            if(song?.collectionName){
              uniqueSongsObject[song.collectionName] = song;
            }
          });
          const uniqueSongsList: ItunesMusicInterface[] = Object.values(uniqueSongsObject);

          // SORT THE SONGS ALPHABETICALLY
          uniqueSongsList.sort((a, b) => {
            return (a.collectionName < b.collectionName) ? -1 : 1
          });

          const finalResult = uniqueSongsList.slice(0, 5);
          // BROADCAST THE SEARCH RESULTS...
          this.vcService.broadcastSearchResults(finalResult);

          // FINALLY, RETURN ONLY THE FIRST 5 RESULTS...
          return finalResult;
        }),
        catchError(this.manageError('fetchSuggestionsByKeyWord', [])),
      );
  }
}
