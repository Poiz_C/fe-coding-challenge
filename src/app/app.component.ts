import {Component, OnInit} from '@angular/core';
import {ValuesCentralService} from './services/values-central.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public shouldShowViewBlock = true;

  constructor(private vcService: ValuesCentralService) {
  }

  ngOnInit(): void {
    this.vcService.searchButtonTriggeredStream.subscribe(() => {
      this.shouldShowViewBlock = !this.shouldShowViewBlock;
    });
  }

}
