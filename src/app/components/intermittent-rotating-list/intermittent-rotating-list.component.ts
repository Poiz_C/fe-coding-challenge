import {Component, OnDestroy, OnInit} from '@angular/core';
import {ItunesMusicInterface} from "../../interfaces/itune-music.interface";
import {Subscription, timer} from "rxjs";
import {ValuesCentralService} from "../../services/values-central.service";

@Component({
  selector: 'app-intermittent-rotating-list',
  templateUrl: './intermittent-rotating-list.component.html',
  styleUrls: ['./intermittent-rotating-list.component.scss']
})
export class IntermittentRotatingListComponent implements OnInit, OnDestroy {
  private _rotatingTunesList: Partial<ItunesMusicInterface>[] = [
    {collectionName: 'A', artworkUrl100: '', },
    {collectionName: 'B', artworkUrl100: '', },
    {collectionName: 'C', artworkUrl100: '', },
    {collectionName: 'D', artworkUrl100: '', },
    {collectionName: 'E', artworkUrl100: '', },
  ];

  private searchResultList: Partial<ItunesMusicInterface>[] = [];
  private tunesRotatingTimer$ = timer(1000, 1000);
  private tunesRotatingSubscription!: Subscription;

  constructor(private vcService: ValuesCentralService) { }

  ngOnInit(): void {
    this.vcService.searchResultsSignal.subscribe((tunes) => {
      this.searchResultList = tunes;
    });

    this.tunesRotatingSubscription = this.tunesRotatingTimer$.subscribe(() => {
      let replaceableItem: Partial<ItunesMusicInterface>;
      if(this.searchResultList.length > 0){
        this.rotatingTunesList.splice(0, 1);
        replaceableItem = this.searchResultList.splice(0, 1)[0];
      }else{
        replaceableItem = this.rotatingTunesList.splice(0, 1)[0];
      }
      this.rotatingTunesList.push(replaceableItem);
    });
  }

  get rotatingTunesList() {
    return this._rotatingTunesList;
  }

  ngOnDestroy(): void {
    this.tunesRotatingSubscription.unsubscribe();
  }

}
