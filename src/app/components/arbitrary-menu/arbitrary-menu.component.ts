import { Component} from '@angular/core';

@Component({
  selector: 'app-arbitrary-menu',
  templateUrl: './arbitrary-menu.component.html',
  styleUrls: ['./arbitrary-menu.component.scss']
})
export class ArbitraryMenuComponent {

  constructor() {}

  preventDefaultAnchorBehavior(event: Event): void {
    event.preventDefault();
  }

}
