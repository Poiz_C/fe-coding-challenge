import { AfterViewInit, Component, ElementRef, Input, ViewChild } from '@angular/core';
import {SearchBrokerService} from '../../services/search-broker.service';
import {ValuesCentralService} from '../../services/values-central.service';
import {Subject} from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import {ItunesMusicInterface} from "../../interfaces/itune-music.interface";

interface EventKeywordInterface {
  event: KeyboardEvent | Event;
  keyword: string;
}

@Component({
  selector: 'app-search-field',
  templateUrl: './search-field.component.html',
  styleUrls: ['./search-field.component.scss']
})
export class SearchFieldComponent implements AfterViewInit {
  @ViewChild('keyword') keyword?: ElementRef;
  @ViewChild('searchResultsComponent') searchResultsComponent?: ElementRef;
  @Input() shouldRenderResult = true;
  public suggestedTunes: ItunesMusicInterface[] = [];
  public shouldFetchData = true;
  private subject: Subject<EventKeywordInterface> = new Subject<EventKeywordInterface>();

  constructor(private searchBrokerService: SearchBrokerService, private vcService: ValuesCentralService) { }

  ngAfterViewInit(): void {
    this.subject.pipe(
      debounceTime(500)
    ).subscribe(eKeyword => {
      this.fetchSuggestedTunes(eKeyword.event as KeyboardEvent, eKeyword.keyword);
    });
    this.vcService.abortOperationsSignal.subscribe(() => {
      if (this.keyword){
        this.keyword.nativeElement.value = '';
        this.resetSearchField( this.keyword.nativeElement);
      }
    });

    this.vcService.refocusSearchFieldSignal.subscribe(() => {
        if (this.keyword) {
          this.keyword.nativeElement.focus();
        }
    });

    this.vcService.updateFieldValueSignal.subscribe((itemValue) => {
      this.updateSearchField(itemValue);
    });
  }

  onKeyUp(event: KeyboardEvent, keyword: string): void {
    this.subject.next({event, keyword});
  }

  fetchSuggestedTunes(event: KeyboardEvent, keyword: string): void {
    switch (event.key) {
      case 'Esc': // IE/EDGE SPECIFIC VALUE
      case 'Escape':
        this.resetSearchField(this.keyword?.nativeElement);
        break;

      case "Enter":
        this.fetchResults(keyword);
        break;

      default:
    }
  }

  fetchResults(keyword: string): void {
    this.shouldFetchData = true;
    this.shouldRenderResult = true;
    keyword = keyword.trim();
    this.suggestedTunes = [];

    this.searchBrokerService.fetchSuggestionsByKeyWord(keyword)
      .subscribe(songs => {
        this.suggestedTunes = songs;
        const lastSearch = this?.keyword?.nativeElement.value;
        this.resetSearchField(this?.keyword?.nativeElement);
        if(this?.keyword) {
          this.keyword.nativeElement.value = lastSearch;
        }
      });
  }

  updateSearchField(strValue: string): void{
    if (this.keyword){
      this.keyword.nativeElement.value = strValue;
    }
  }

  resetSearchField(keywordField: HTMLInputElement): void{
    keywordField.value = '';
    this.shouldFetchData = false;
    this.shouldRenderResult = false;
    this.suggestedTunes = [];
  }

  repositionLabel(labelTextNode: HTMLElement , actionType: string): void{
    switch (actionType.toLowerCase()){
      case 'blur':
        if (labelTextNode.classList.contains('search-label__label-text--entered')) {
          if (!this.keyword?.nativeElement.value){
            labelTextNode.classList.remove('search-label__label-text--entered');
          }
        }
        break;
      case 'focus':
        if (!labelTextNode.classList.contains('search-label__label-text--entered')) {
          labelTextNode.classList.add('search-label__label-text--entered');
        }
        break;
    }
  }
}
